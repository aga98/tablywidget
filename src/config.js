export default function () {
  const config = {
    message: 'Hey Du!😁 <br/> Naa hunger? <br/>Reserviere jetzt!!!',
    reservationTime: ['16:00', '17:00', '18:00', '19:00', '20:00'],
    restaurantName: 'Schulhaus',
    hasChildren: true,
    hasMenu: true,
    color: '#444',
  };

  return config;
}
