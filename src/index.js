import $ from 'jquery';
import 'bootstrap';
import 'bootstrap-datepicker';
import 'sass/styles.scss';
import getMarkup from 'markup';

const body = document.getElementsByTagName('body')['0'];
const timeSelect = document.getElementsByClassName('tably-widget-radio-time-value');
const data = [];

// BASE WIDGET / MESSAGE from Restaurant
class Widget {
  index() {

  }

  toggle() {
    $('.tably-widget-wrapper').toggleClass('tably-widget-wrapper-show');
  }
}

// FOR DATE
class Date {
  index() {
    $('.tably-widget-datepicker').datepicker({
      format: 'dd.mm.yyyy',
      todayHighlight: true,
    });
    this.btn();
  }

  btn() {
    $('.tably-widget-datepicker').on('changeDate', () => {
      $('.tably-widget-input-datepicker').val($('.tably-widget-datepicker').datepicker('getFormattedDate'));
      data.date = $('.tably-widget-input-datepicker').val();
      document.getElementById('tably-widget-container-date').classList.add('tably-widget-container-hidden');
      document.getElementById('tably-widget-container-time').classList.remove('tably-widget-container-hidden');
    });
  }
}

// FOR TIME
class Time {
  index() {
    this.btn();
  }

  btn() {
    const backBtn = document.getElementById('tably-widget-time-btn-back');

    [].forEach.call(timeSelect, (time) => {
      time.addEventListener('click', () => {
        data.time = $(time).val();
        document.getElementById('tably-widget-container-time').classList.add('tably-widget-container-hidden');
        document.getElementById('tably-widget-container-person').classList.remove('tably-widget-container-hidden');
      });
    });

    backBtn.addEventListener('click', () => {
      document.getElementById('tably-widget-container-date').classList.remove('tably-widget-container-hidden');
      document.getElementById('tably-widget-container-time').classList.add('tably-widget-container-hidden');
    });
  }
}

// FOR Person
class Person {
  index() {
    this.btn();
  }

  btn() {
    const nextBtn = document.getElementById('tably-widget-person-btn-next');
    const backBtn = document.getElementById('tably-widget-person-btn-back');

    $('.tably-widget-select-person').on('change', () => {
      data.person = $('.tably-widget-select-person').val();
    });

    backBtn.addEventListener('click', () => {
      document.getElementById('tably-widget-container-time').classList.remove('tably-widget-container-hidden');
      document.getElementById('tably-widget-container-person').classList.add('tably-widget-container-hidden');
    });

    nextBtn.addEventListener('click', () => {
      document.getElementById('tably-widget-container-person').classList.add('tably-widget-container-hidden');
      document.getElementById('tably-widget-container-customer').classList.remove('tably-widget-container-hidden');
    });
  }
}

// FOR Customer
class Customer {
  index() {
    this.btn();
  }

  btn() {
    const nextBtn = document.getElementById('tably-widget-customer-btn-next');
    const backBtn = document.getElementById('tably-widget-customer-btn-back');

    backBtn.addEventListener('click', () => {
      document.getElementById('tably-widget-container-person').classList.remove('tably-widget-container-hidden');
      document.getElementById('tably-widget-container-customer').classList.add('tably-widget-container-hidden');
    });

    nextBtn.addEventListener('click', () => {
      data.vorname = $('.tably-widget-customer-vorname').val();
      data.name = $('.tably-widget-customer-name').val();
      data.email = $('.tably-widget-customer-email').val();
      data.phone = $('.tably-widget-customer-phone').val();
      document.getElementById('tably-widget-container-customer').classList.add('tably-widget-container-hidden');
      document.getElementById('tably-widget-container-finish').classList.remove('tably-widget-container-hidden');
    });
  }
}

const date = new Date();
const time = new Time();
const widget = new Widget();
const person = new Person();
const customer = new Customer();

const intialize = () => {
  body.innerHTML = getMarkup(data);
  date.index();
  widget.index();
  person.index();
  time.index();
  customer.index();
};

intialize();

document.getElementById('tably-widget-notification').addEventListener('click', () => {
  widget.toggle();
});

document.getElementById('tably-widget-toggle').addEventListener('click', () => {
  widget.toggle();
});

