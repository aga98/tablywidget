import getConfig from './config';
import markupDate from './markup/date';
import markupTime from './markup/time';
import markupPerson from './markup/person';
import markupCustomer from './markup/customer';
import markupFinish from './markup/finish';

export default function (data) {
  console.log(data);
  const config = getConfig();
  const widgetToggle = '<div class="tably-widget-toggle" id="tably-widget-toggle">T</div>';
  const widgetClose = '<div class="tably-widget-close">X</div>';
  const widgetHeader = `<div class="tably-widget-header">${widgetClose}</div>`;
  const date = markupDate();
  const person = markupPerson();
  const customer = markupCustomer();
  const time = markupTime();
  const finish = markupFinish(data);
  const widgetinner = `<div class="tably-widget-inner">${date}${time}${person}${customer}${finish}</div>`;
  const widgetWrapper = `<div class="tably-widget-wrapper" id="tably-widget-wrapper">${widgetHeader} ${widgetinner}</div>`;
  const widget = widgetWrapper;
  const widgetNotification = `<div class="tably-widget-notification" id="tably-widget-notification">${config.message}</div>`;

  return widgetToggle + widgetNotification + widget;
}
