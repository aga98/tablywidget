import 'bootstrap-datepicker';

export default function () {
  const btnBack = '<button class="tably-widget-btn-back" id="tably-widget-customer-btn-back">< Zurück</button>';

  return `
      <div id="tably-widget-container-customer" class="tably-widget-container-hidden">
          <label>Vorname</label>
          <input type="text" class="tably-widget-input tably-widget-customer-vorname">
          <label>Nachname</label>
          <input type="text" class="tably-widget-input tably-widget-customer-name">
          <label>Email</label>
          <input type="email" class="tably-widget-input tably-widget-customer-email">
          <label>Mobiltelefon</label>
          <input type="text" class="tably-widget-input tably-widget-customer-phone">
          <button class="tably-widget-btn-next" id="tably-widget-customer-btn-next">Weiter</button>
          ${btnBack}
       </div>
    `;
}

