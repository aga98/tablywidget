import 'bootstrap-datepicker';
import getConfig from '../config';

export default function () {
  const config = getConfig();
  const array1 = config.reservationTime;
  let markupInner = '';
  const btnBack = '<button class="tably-widget-btn-back" id="tably-widget-time-btn-back">< Zurück</button>';

  array1.forEach((time) => {
    markupInner += `<label class="tably-widget-radio-time"><input class="tably-widget-radio-time-value" type="radio" value="${time}" name="time"/>${time}</label>`;
  });

  const markup = `<div id="tably-widget-container-time" class="tably-widget-container-hidden">${markupInner}${btnBack}</div>`;

  return markup;
}
