import 'bootstrap-datepicker';

export default function () {
  const btnBack = '<button class="tably-widget-btn-back" id="tably-widget-person-btn-back">< Zurück</button>';

  return `
      <div id="tably-widget-container-person" class="tably-widget-container-hidden">
        <label>Personen</label>
          <select class="tably-widget-select-person">
            <option value="1">1 Personen</option>
            <option value="2">2 Personen</option>
            <option value="3">3 Personen</option>
          </select>
          <button class="tably-widget-btn-next" id="tably-widget-person-btn-next">Weiter</button>
          ${btnBack}
       </div>
    `;
}

