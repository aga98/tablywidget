import 'bootstrap-datepicker';

export default function () {
  return `
      <div id="tably-widget-container-date">
          <div class="tably-widget-datepicker" data-date="12/03/2012"></div>
          <input type="hidden" class="tably-widget-input-datepicker">
        </div>
    `;
}

