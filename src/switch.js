export default function () {
  const toggleTime = () => {
    document.getElementById('tably-widget-container-time').classList.add('tably-widget-container-hidden');
  };

  const time = document.getElementsByClassName('tably-widget-radio-time');

  [].forEach.call(time, (item) => {
    item.addEventListener('click', () => {
      toggleTime();
    });
  });
}
